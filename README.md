# NixTemplates
A repository with Nix flake templates for computational chemistry.
For an introduction to refer to the excellent (although slightly outdated) articles from [Serokell](https://serokell.io/blog/practical-nix-flakes) and [Tweag.io](https://www.tweag.io/blog/2020-05-25-flakes/).
For a general introduction to Nix there is no workaround for going through the [Nix Pills](https://nixos.org/guides/nix-pills/) and/or [nix.dev](https://nix.dev/).

## General Hints
Applications with OpenGL acceleration and CUDA programmes on non-NixOS machines require nixGL to utilise the graphics driver of the machine.
```bash
nix profile install --override-input nixpkgs nixpkgs/nixos-unstable --impure github:nix-community/nixGL
```
For detailed instructions see the [nixGL repository](https://github.com/nix-community/nixGL).

For Ara, use the NixGL version from this flake `nixGLAra` and install impure via
```bash
nix profile install --impure gitlab:theoretical-chemistry-jena/nixtemplates#nixGLAra
```

## Usage
Templates can be accessed via `nix flake init -t gitlab:theoretical-chemistry-jena/nixtemplates#$TEMPLATE`.
Below available templates are described that can be used as `$TEMPLATE`.
You may also use `nix flake show gitlab:theoretical-chemistry-jena/nixtemplates` to inspect the contents of the flake.

### `computationalChemistry`
A flake for computational chemistry, both for quantum mechanics and molecular mechanics as well as MD.
```
nix flake init -t gitlab:theoretical-chemistry-jena/nixtemplates#computationalChemistry
```
This flake should reside __outside__ of the directory in which the project is run and stores its data!
A single large Python environment is defined that provides JupyterLab and a set of computational chemistry libraries such as `pysisyphus`, `xtb-python`, `gpaw`, ...
Furthermore, a builder for Singularity containers is provided in case you are required to run a programme on a non-nixified machine.
Those are accessible via `legacyPackages.singularityPkgs.$PKG` where `$PKG` is one of the packages available in `packages`, e.g. `nix build .#singularityPkgs.molcas`.

#### Submit Scripts
The `computationalChemistry` flake contains submit scripts suitable for the Ara and Aurora clusters.
Install them via
```bash
nix profile install gitlab:theoretical-chemistry-jena/nixtemplates?dir=computationalChemistry#submit.{nwchem,orca,psi4,cp2k,gaussian,pysisyphus}
```
(Yes, I'm sorry for this monster of a command ...) to obtain submit scripts for the respective programs.
You should have commands like `orca-submit`, `pysisyphus-submit`, etc. in your path now and can obtain some usage instructions via e.g. `orca-submit -?`.

These scripts are meant to be used as `sbatch` submit scripts and all follow the same logic:

  * Generate the actual input file with correct ressource allocations for your program from a [Mustache template](https://mustache.github.io/mustache.5.html) instead of using `sed` or `awk` magic or grepping and checking such values (e.g number of CPUs, RAM, scratch directory) in an existing input file.
  * Use an existing flake, e.g. [Nix-QChem](https://github.com/Nix-QChem/NixOS-QChem), [NixPkgs](https://github.com/NixOS/nixpkgs), or a local flake from e.g. this repository as source for your quantum chemistry program.

For example, consider a simple ORCA job for EOM-CCSD.
An appropriate input __template__ file `water_eomccsd.tmpl` could look like this:

```
! rhf eom-ccsd cc-pvtz rijcosx def2/j cc-pvtz/c
%maxcore {{ MEM_PER_PROCESS }}
%pal
  nprocs {{ SLURM_NTASKS }}
end
%mdci
  nroots 2
end
*xyzfile 0 1 water.xyz
```
Here, ` {{ MEM_PER_PROCESS }}` and `{{ SLURM_NTASKS }}` are Mustache variables, that will be replaced with values as required by SLURM and the submit script.
Reasonable Mustache variable names suggested for each of the programs can be listed via the `-?` flag.
To submit the calculation, use the following command
```bash
sbatch -N 1 --ntasks-per-node=24 -p b_standard -t 10:00:00 -J water-eomccsd --mem=60G orca-submit -i water_eomccsd.tmpl -f $FLAKE_URI water.xyz
```
Note, that all submit scripts take the `-i` argument for the main input file and `-f` to point to a flake (a relative or absolute path to a directory, or a git URL containing `flake.nix`, see [here](https://nix.dev/manual/nix/2.18/command-ref/new-cli/nix3-flake#examples)), that provides the program to run, in this case ORCA.
The ORCA submit script takes additional files required to run the calculation as additional arguments, in this case `water.xyz`.

All submit scripts will take care of ensuring reasonable scratch directory handling and sane resource allocations, given you use the Mustache variables instead of writing resource allocations yourself!

Remember, that other submit scripts may use additional, less, or other Mustache variables!

Adding other submit scripts is explicitly encouraged.
The [`common.sh`](https://gitlab.com/theoretical-chemistry-jena/NixTemplates/-/blob/112d63fb3fd86ea6849dbd79ccecf5053d71ad25/computationalChemistry/pkgs/submit/common.sh) contains useful bash functions to scale down resources such as RAM, handle scratch directory creation and getting files back and so on.
All existing submit scripts are built around this infrastructure.

### `haskell`
A flake for developing a Haskell package.
```
nix flake init -t gitlab:theoretical-chemistry-jena/nixtemplates#haskell
```
Development tools are included by means of `nix develop` and a [`direnv`](https://direnv.net/) integration.
That is, if direnv is configured on your system, switching with a shell into the repository is enough to obtain Haskell libraries, cabal, the GHC, the Haskell-Language-Server, Fourmolu and so on.
Remember to generate the `*.cabal` file by running `hpack`.

### `fortran`
A basic template for Fortran projects.
Uses Meson (with Ninja) as the build tool, and provides the `fprettify` formatter.

```
nix flake init -t gitlab:theoretical-chemistry-jena/nixtemplates#fortran
```

After initializing the project, consider running the provided `setup.sh` script.
It will set the name of your project in all relevant files.
Invoke as follows: `./setup.sh myProjectName`.
Feel free to delete the script afterwards.

Whenever you add new source files or otherwise change the structure of your project, you will have to update the `meson.build` file.
If you don't want to do this manually, you can automatically generate it by running
```
meson init -f -n yourProjectName [source files]
```
…where `yourProjectName` is the name of your project, obviously.
Nix knows how to handle the build process from there.

If you want to build manually, you will need `meson setup` and `meson compile`.
For more on meson, refer to the [documentation](https://mesonbuild.com/Quick-guide.html)
