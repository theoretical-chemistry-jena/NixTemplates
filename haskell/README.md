# Example Package
Replace all references to `package` by your actual package name.
Development tools are available via `nix develop` and the corresponding `direnv` integration.
VisualStudio Codium/Code can be configured to use the direnv environment and find these dependencies.