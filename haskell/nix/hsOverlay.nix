# `final` and `prev` refer to the top-level overlay at the nixpkgs level. Thus,
# this overlay can directly applied to nixpkgs.
final: prev:

{
  haskell = prev.haskell // {
    packageOverrides = hfinal: hprev: prev.haskell.packageOverrides hfinal hprev // {
      package = hprev.callCabal2nixWithOptions "package" ../. "" { };
    };
  };
}
