{
  description = "A nixified Haskell project";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    nixBundlers.url = "github:NixOS/bundlers/master";
  };

  nixConfig = {
    allow-import-from-derivation = "true";
  };

  outputs = { self, nixpkgs, flake-utils, nixBundlers }: flake-utils.lib.eachSystem [ "x86_64-linux" ]
    (system:
      let
        hsOvl = import ./nix/hsOverlay.nix;

        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
          overlays = [ hsOvl ];
        };

      in
      {
        packages = {
          default = self.packages."${system}".package;
          package = pkgs.haskellPackages.package;
          package_static = pkgs.pkgsStatic.haskellPackages.package;
        };

        devShells.default = pkgs.haskellPackages.shellFor {
          withHoogle = true;
          packages = p: [ p.package ];
          buildInputs = with pkgs; [
            cabal-install
            cabal2nix
            haskell-language-server
            #haskellPackages.hls-fourmolu-plugin
            haskellPackages.fourmolu
            hlint
            hpack
            nixpkgs-fmt
          ];
        };

        bundlers = {
          inherit (nixBundlers.bundlers."${system}") toArx toDEB toRPM toDockerImage;
          default = self.bundlers."${system}".toArx;
        };        

        checks = self.packages."${system}";
      }) // {
    overlays.default = import ./nix/hsOverlay.nix;

    hydraJobs."x86_64-linux" = self.checks."x86_64-linux";
  };
}
