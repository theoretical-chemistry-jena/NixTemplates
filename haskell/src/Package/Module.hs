module Package.Module (
  packageMain
) where 

import RIO

packageMain :: IO ()
packageMain = runSimpleApp . logInfo $ "Hello Sheep"