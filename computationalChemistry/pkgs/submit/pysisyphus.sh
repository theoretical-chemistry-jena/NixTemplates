function usage() {
  echo "Usage: $0 -i TEMPLATE_FILE -f FLAKE" 1>&2
  echo "Mustache variables:"
  echo "  MEM_PER_PROCESS"
  echo "  SLURM_NTASKS"
  exit 1
}

while getopts ":i:f:" opt; do
  case "${opt}" in
    i )
      TEMPLATE_FILE=$OPTARG
      ;;
    f )
      FLAKE=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument"
      usage
      ;;
  esac
done

if [ -z "$TEMPLATE_FILE" ] || [ -z "$FLAKE" ]
  then usage
fi

createScratchDir > /dev/null
export TMPDIR=$SCRATCH_DIR
getMemPerProcess > /dev/null

trap 'rm -rf $SCRATCH_DIR' EXIT ERR SIGINT SIGTERM

mkInputFromTemplate "$TEMPLATE_FILE" "$(mktemp --suffix=".yaml")"

nix shell "$FLAKE"#pysisyphus --command pysis "$INPUT_FILE" > "$SLURM_SUBMIT_DIR/${TEMPLATE_FILE%.*}.out"