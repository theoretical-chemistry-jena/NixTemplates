function usage() {
  echo "Usage: $0 -i TEMPLATE_FILE -s STRUCTURE_FILE -f FLAKE" 1>&2
  echo "Mustache variables:"
  echo "  MEM_PER_PROCESS"
  exit 1
}

while getopts ":i:s:f:" opt; do
  case "${opt}" in
    i )
      TEMPLATE_FILE=$OPTARG
      ;;
    s )
      STRUCTURE_FILE=$OPTARG
      ;;
    f )
      FLAKE=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument"
      usage
      ;;
  esac
done

if [ -z "$TEMPLATE_FILE" ] || [ -z "$STRUCTURE_FILE" ] || [ -z "$FLAKE" ]
  then usage
fi

if [ -z "$SLURM_NTASKS" ]
  then
    echo "SLURM_NTASKS not set. Set a number of tasks when submitting the job."
    exit 1
fi

export OMP_NUM_THREADS=${SLURM_NTASKS},1

getMemPerProcess > /dev/null
mkInputFromTemplate "$TEMPLATE_FILE" "$(mktemp)" > /dev/null

trap 'rm -rf $INPUT_FILE' EXIT ERR SIGINT SIGTERM

nix shell "$FLAKE"#xtb --command xtb "$STRUCTURE_FILE" --input "$INPUT_FILE" > xtb.out
