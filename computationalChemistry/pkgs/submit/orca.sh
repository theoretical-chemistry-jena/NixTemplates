function usage() {
  echo "Usage: $0 -i TEMPLATE_FILE -f FLAKE ADDITIONAL_INPUT1 ADDITIONAL_INPUT2 ..." 1>&2
  echo "Mustache variables:"
  echo "  MEM_PER_PROCESS"
  echo "  SLURM_NTASKS"
  exit 1
}

while getopts ":i:f:" opt; do
  case "${opt}" in
    i )
      TEMPLATE_FILE=$OPTARG
      ;;
    f )
      FLAKE=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument"
      usage
      ;;
  esac
done

if [ -z "$TEMPLATE_FILE" ] || [ -z "$FLAKE" ]
  then usage
fi

EXTRA_INPUTS=("${@:OPTIND}")

createScratchDir > /dev/null
populateScratchDir "${EXTRA_INPUTS[@]}" > /dev/null
getMemPerProcess > /dev/null

ORCA_TMP_PATTERNS=("*.tmp*" "*.tmp" "tmp.*" "*.inp")

trap 'getOutputsFromScratchDir ${ORCA_TMP_PATTERNS[@]} && rm -rf $SCRATCH_DIR' EXIT ERR SIGINT SIGTERM

mkInputFromTemplate "$TEMPLATE_FILE" "$SCRATCH_DIR/${TEMPLATE_FILE%.*}.inp"

ABS_FLAKE=$(realpath "$FLAKE")
cd "$SCRATCH_DIR"
nix shell "$ABS_FLAKE"#orca --command orca "$INPUT_FILE" > "$SLURM_SUBMIT_DIR/${TEMPLATE_FILE%.*}.out"