function usage() {
  echo "Usage: $0 -i TEMPLATE_FILE -f FLAKE" 1>&2
  echo "Mustache variables:"
  echo "  MEM_PER_PROCESS"
  echo "  SLURM_NTASKS"
  exit 1
}

while getopts ":i:f:" opt; do
  case "${opt}" in
    i )
      TEMPLATE_FILE=$OPTARG
      ;;
    f )
      FLAKE=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument"
      usage
      ;;
  esac
done

if [ -z "$TEMPLATE_FILE" ] || [ -z "$FLAKE" ]
  then usage
fi

export OMP_NUM_THREADS=$SLURM_NTASKS

createScratchDir > /dev/null
getMemPerProcess > /dev/null
mkInputFromTemplate "$TEMPLATE_FILE" "$(mktemp)" > /dev/null

export TMPDIR=$SCRATCH_DIR

trap 'rm -rf $SCRATCH_DIR $INPUT_FILE' EXIT ERR SIGINT SIGTERM

nix shell "$FLAKE"#gaussian --command g16 < "$INPUT_FILE" > "${TEMPLATE_FILE%.*}".log
