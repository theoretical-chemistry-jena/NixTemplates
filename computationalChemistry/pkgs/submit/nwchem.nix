{ writeShellApplication
, runtimeShell
, mo
, getopt
, bc
}:

writeShellApplication {
  name = "nwchem-submit";
  runtimeInputs = [ mo getopt bc ];
  excludeShellChecks = [ "SC2155" "SC2119" "SC2120" ];
  bashOptions = [ "errexit" "errtrace" "pipefail" ];
  text = ''
    ${builtins.readFile ./common.sh}
    ${builtins.readFile ./nwchem.sh}
  '';
}