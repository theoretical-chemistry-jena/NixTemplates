# Get memory per PROCESS from SLURM
#
# Environment:
#   SLURM_MEM_PER_CPU || (SLURM_MEM_PER_NODE && (SLURM_NTASKS_PER_NODE || SLURM_NTASKS))
#
# Arguments:
#   $1, optional: Factor, 0.8 by default
#   $2, optional: Threads per Task, 1 by default
#
# Output:
#    MEM_PER_PROCESS
function getMemPerProcess() {
  # Check if input arguments are provided or set defaults
  if [ -v 1 ]
    then _MEM_FACTOR=$1
    else _MEM_FACTOR="0.8"
  fi
  if [ -v 2 ]
    then _THREADS_PER_TASK=$2
    else _THREADS_PER_TASK="1"
  fi

  # Check for required SLURM environment variables
  if [ -v SLURM_MEM_PER_CPU ]
    then export MEM_PER_PROCESS=$(echo "($SLURM_MEM_PER_CPU * $_THREADS_PER_TASK * $_MEM_FACTOR) / 1" | bc)
  elif [ -v SLURM_MEM_PER_NODE ] && [ -v SLURM_NTASKS_PER_NODE ]
    then export MEM_PER_PROCESS=$(echo "($SLURM_MEM_PER_NODE * $_MEM_FACTOR * $_THREADS_PER_TASK / $SLURM_NTASKS_PER_NODE) / 1" | bc)
  elif [ -v SLURM_MEM_PER_NODE ] && [ -v SLURM_NTASKS ]
    then export MEM_PER_PROCESS=$(echo "($SLURM_MEM_PER_NODE * $_MEM_FACTOR * $_THREADS_PER_TASK / $SLURM_NTASKS) / 1" | bc)
  else
    echo "Error: SLURM environment variables not set"
    exit 1
  fi
  echo "$MEM_PER_PROCESS"
}

# Calculate the number of processes per node
#
# Environment:
#   OMP_NUM_THREADS (optional, assume no SMP/OMP_NUM_THREADS=1 otherwise)
#   SLURM_NTASKS_PER_NODE || SLURM_NTASKS && SLURM_NNODES
#
# Outputs:
#   PROCESSES_PER_NODE
function getProcessesPerNode() {
  # Check if OMP_NUM_THREADS is set
  if [ -v OMP_NUM_THREADS ]
    then _OMP_NUM_THREADS=$OMP_NUM_THREADS
    else _OMP_NUM_THREADS="1"
  fi

  # Check for required SLURM environment variables
  if [ -v SLURM_NTASKS_PER_NODE ]
    then export PROCESSES_PER_NODE=$(( SLURM_NTASKS_PER_NODE / _OMP_NUM_THREADS ))
  elif [ -v SLURM_NTASKS ] && [ -v SLURM_NNODES ]
    then export PROCESSES_PER_NODE=$(( SLURM_NTASKS / SLURM_NNODES / _OMP_NUM_THREADS))
  else
    echo "Error: SLURM environment variables not set"
    exit 1
  fi
  if [ "$PROCESSES_PER_NODE" -eq 0 ]
    then PROCESSES_PER_NODE=1
  fi
  echo "$PROCESSES_PER_NODE"
}

# Create a scratch directory
#
# Arguments:
#   $1, optional: Scratch parent directory, /beegfs/$USER by default
#
# Output:
#   SCRATCH_DIR
function createScratchDir() {
  # Check if input argument is provided or set default
  if [ -v 1 ]
    then _SCRATCH_PARENT=$1
    else _SCRATCH_PARENT="/beegfs/$USER"
  fi

  [[ -d "${_SCRATCH_PARENT}" ]] || mkdir -p "${_SCRATCH_PARENT}"

  # Create a scratch directory
  export SCRATCH_DIR=$(mktemp -d -p "${_SCRATCH_PARENT}")

  echo "$SCRATCH_DIR"
}

# Create an input file from a template
#
# Arguments:
#   $1: Template file
#   $2: Output file
#
# Output:
#   INPUT_FILE
function mkInputFromTemplate() {
  # Check if input arguments are provided
  if [ -v 1 ]
    then _TEMPLATE_FILE=$1
    else
      echo "Error: No template file provided"
      exit 1
  fi

  if [ -v 2 ]
    then INPUT_FILE=$2
    else
      echo "Error: No path for actual input file provided"
      exit 1
  fi

  mo -u "$_TEMPLATE_FILE" > "$INPUT_FILE"

  echo "$INPUT_FILE"
}

# Move relevant inputs to scratch directory
#
# Environment:
#   SCRATCH_DIR
#
# Arguments:
#   $1: array of files to get to scratch directory
function populateScratchDir() {
  # Check if input arguments are provided
  if [ -v SCRATCH_DIR ]
    then _FILES=("$@")
    else
      echo "Error: No files provided"
      exit 1
  fi

  # Move files to scratch directory
  for file in "${_FILES[@]}"; do
    cp "$file" "$SCRATCH_DIR"
  done
}

# Get relevant outputs back from scratch directory
#
# Environment:
#   SCRATCH_DIR
#   SLURM_SUBMIT_DIR
#
# Arguments:
#   $1: array of patterns of files NOT to get back from scratch directory
function getOutputsFromScratchDir() {
  # Check if variables are set
  if [ -v SCRATCH_DIR ] && [ -v SLURM_SUBMIT_DIR ]
    then _PATTERNS=("$@")
    else
      echo "Error: Required variables not set"
      exit 1
  fi

  # Get files from scratch directory
  PATGEN=""
  for PAT in "${_PATTERNS[@]}"; do
    PATGEN="$PATGEN ! -name \"$PAT\""
  done

  bash -c "find $SCRATCH_DIR/ $PATGEN ! -type l -exec cp -r {} $SLURM_SUBMIT_DIR/. \;"
}