function usage() {
  echo "Usage: $0 -i TEMPLATE_FILE -p THREADS_PER_PROCESS -f FLAKE" 1>&2
  echo "Mustache variables:"
  echo "  MEM_PER_PROCESS"
  exit 1
}

while getopts ":i:p:f:" opt; do
  case "${opt}" in
    i )
      TEMPLATE_FILE=$OPTARG
      ;;
    p )
      THREADS_PER_PROCESS=$OPTARG
      ;;
    f )
      FLAKE=$OPTARG
      ;;
    \? )
      usage
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument"
      usage
      ;;
  esac
done

if [ -z "$TEMPLATE_FILE" ] || [ -z "$THREADS_PER_PROCESS" ] || [ -z "$FLAKE" ]
  then usage
fi

getMemPerProcess > /dev/null

trap 'rm -rf $SCRATCH_DIR $INPUT_FILE' EXIT ERR SIGINT SIGTERM

mkInputFromTemplate "$TEMPLATE_FILE" "$(mktemp)" > /dev/null

export OMP_NUM_THREADS=$THREADS_PER_PROCESS

nix shell "$FLAKE"#cp2k --command mpiexec -N "$(getProcessesPerNode)" cp2k.psmp "$INPUT_FILE" > "${TEMPLATE_FILE%.*}".out
