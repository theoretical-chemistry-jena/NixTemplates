{
  description = ''

    Computational Chemistry Flake
    -----------------------------

    This flake provides packages and environments for computational chemistry,
    that is quantum chemistry, molecular mechanics, MD and some quantum
    dynamics. Use 
    
        nix flake show [ $PATH_TO_FLAKE_DIR ]

    to inspect the contents of the flake and

        nix flake metadate [ $PATH_TO_FLAKE_DIR ]

    to inspect version pins, last time of update and so on. Obtain an 
    interactive shell with packages available via
    
        nix shell $PATH_TO_FLAKE_DIR#$PACKAGE1 [ $PATH_TO_FLAKE_DIR#$PACKAGE2 ... ]
    
    e.g.

        nix shell ~/flakes/compchem#pyCompChem

    to obtain a python environment with common packages for common workflows.
    Alternatively directly run a programme via

        nix run $PATH_TO_FLAKE_DIR#PACKAGE -- $ARGUMENTS_TO_PROGRAMME
    
    e.g.
    
        nix run ~/flakes/compchem#xtb -- -ohess molecule.xyz
    
    to run xtb.
    
    Note the double dash to separate programme arguments from arguments to nix
    commands.
    Multiple package formats, including singularity images can be obtained via
    Nix-Bundlers, e.g.

        nix bundle --bundler .#toSingularityImage .#pysisyphus
        nix bundle --bundler .#toArx .#molcas
        nix bundle --bundler .#toRPM .#cp2k
    
    For singularity usage have a look at the excellent singularity tutorial at
    https://singularity-tutorial.github.io/.
  '';

  inputs = {
    qchem.url = "github:nix-qchem/nixos-qchem/master";

    flake-utils.url = "github:numtide/flake-utils";

    nixBundlers.url = "github:NixOS/bundlers/master";

    multiwfn.url = "git+https://gitlab.com/theoretical-chemistry-jena/quantum-chemistry/multiwfn?ref=gfortran";

    pysisyphus.url = "github:eljost/pysisyphus";
  };

  outputs = { self, qchem, flake-utils, nixBundlers, multiwfn, pysisyphus }: flake-utils.lib.eachSystem [ "x86_64-linux" ]
    (system:
      let
        # Uses MPICH instead of OpenMPI
        mpichOvl = final: prev: {
          mpich = prev.mpich.overrideAttrs (old: rec {
            version = "4.0.3"; # >= 4.1 breaks CP2K
            src = prev.fetchurl {
              url = "https://www.mpich.org/static/downloads/${version}/mpich-${version}.tar.gz";
              sha256 = "sha256-F0BuqQpu1OzVvjnJ3cv6yTQ+arT3esToxevko+O2xQE=";
            };
          });

          mpi = final.mpich;
        };

        # Uses MVAPICH2 instead of OpenMPI
        mvapichOvl = final: prev: {
          mpi = final.mvapich;

          scalapack = prev.scalapack.overrideAttrs {
            doCheck = false;
          };
        };

        # Use Intel MKL instead of OpenBLAS
        mklOvl = final: prev: {
          blas = prev.blas.override { blasProvider = final.mkl; };
          lapack = prev.lapack.override { lapackProvider = final.mkl; };
        };

        # Add this overlay to use Gaussian on Ara
        gaussianAra = final: prev: {
          qchem = prev.qchem // {
            gaussian = prev.callPackage "${qchem}/pkgs/apps/gaussian" {
              optpath = null;
              version = "16c02";
              # Ara specific paths
              g16Root = "/cluster/apps/gaussian/g16_c02";
              g16Dir = "/cluster/apps/gaussian/g16_c02/g16";
            };
          };
        };

        # Add this overlay to use Gaussian on OpenSuse machines
        gaussianSuse = final: prev: {
          qchem = prev.qchem // {
            gaussian = prev.callPackage "${qchem}/pkgs/apps/gaussian" {
              optpath = "/usr/remote";
              version = "16";
            };
          };
        };

        # Override versions in NixOS-QChem with tracked flake versions
        upstreamOvl = final: prev: {
          qchem = prev.qchem // {
            multiwfn = multiwfn.packages."${system}".default;

            python3 = prev.qchem.python3.override (old: {
              packageOverrides = prev.lib.composeExtensions (old.packageOverrides or (_: _: { })) (pfinal: pprev: {
                pysisyphus = final.python3.pkgs.pysisyphus.override {
                  enableOrca = true;
                  enableTurbomole = true;
                  enableGaussian = final.qchem.gaussian != null;
                  gaussian = final.qchem.gaussian;
                  multiwfn = final.qchem.multiwfn;
                  pytestCheckHook = null;
                  xtb-python = null;
                };
              });
            });
          };
        };

        pkgs = import qchem.inputs.nixpkgs {
          inherit system;
          # To change the MPI or BLAS implementation include additional overlays
          # here, e.g. to use MVAPICH instead of OpenMPI include `mvapichOvl`.
          # Note: These overlays may break some packages, e.g. the MKL overlay
          # breaks numpy and scipy.
          overlays = [
            qchem.overlays.default
            pysisyphus.overlays.default
            upstreamOvl
          ];
          config = {
            allowUnfree = true;
            qchem-config = {
              # Target CPU generation for optimisations such as vectorisation.
              # Only available on x86, see https://gcc.gnu.org/onlinedocs/gcc/x86-Options.html
              # for a list of possible options 
              optArch = "haswell";

              # Access to some propietrary codes on the workgroup server. May be
              # used within the institute network but will produce errors outside.
              #srcurl = "https://nix-serve.ipc3.uni-jena.de/nix-src";

              # Enables CUDA support in some packages. This requires most often a
              # NixOS machine and certainly a recent NVidia GPU.
              useCuda = false;

              # Absolute path to a Q-Chem license file obtained via Mail. See also
              # https://github.com/markuskowa/NixOS-QChem#q-chem, e.g.
              # licQChem = "/home/phillip/.qchem.license.dat";

              allowEnv = false;
            };
          };
        };

        # A Python development environment for quantum chemistry.
        pyCompChem = pkgs.qchem.python3.withPackages (p: with p; [
          jupyterlab
          numpy
          scipy
          scikit-learn
          openmm
          gpaw
          pyscf
          ase
          pandas
          psi4
          pkgs.qchem.python3.pkgs.pysisyphus
          polyply
          adcc
          cclib
          mdtraj
          molbar
        ]);

        # An tweaked singularity builder to build singularity images for 
        # computational chemistry software.
        toSingularityImage = drv: pkgs.singularity-tools.buildImage {
          name = drv.name;
          contents = with pkgs; [
            bashInteractive
            coreutils
            findutils
            gnused
            which
            drv
          ];

          # Size of the virtual disk used for building the container.
          # This is NOT the final disk size.
          diskSize = 40000;

          # Memory for the virtualisation environment that BUILDS the image.
          # This is not a runtime parameter of the image.
          memSize = 6000;
        };
      in
      {
        packages = {
          inherit (pkgs.qchem)
            # Visualisation and Analysis
            avogadro2 vmd gabedit wxmacmolplt gaussview iboview janpa luscus multiwfn
            pegamoid molden orient gdma theodore travis-analyzer wfoverlap

            # System Setup
            moltemplate packmol pdbfixer polyply

            # Quantum Chemistry
            bagel cfour cp2k dalton dirac dice ergoscf exciting gamess-us
            macroqc mopac mrcc mrchem nwchem octopus molcas molcasDice orca psi4
            quantum-espresso qmcpack salmon sgroup siesta turbomole veloxchem xtb xtb-iff

            # Molecular Mechanics
            gromacs gromacsMpi openmm tinker ambertools

            # Optimisation and Dynamics
            autodock-vina crest pysisyphus i-pi sharc sharc-molcas sharc-bagel
            sharc-orca sharc-turbomole sharc-full
            ;

          # Packages directly from upstream.
          inherit (pkgs) lammps lammps-mpi povray tachyon mo molbar;

          # Python environment for computational chemistry. Use via "nix shell"
          inherit pyCompChem;
        } // pkgs.lib.attrsets.optionalAttrs (pkgs.qchem.q-chem != null) {
          inherit (pkgs.qchem) q-chem q-chem-installer;
        } // pkgs.lib.attrsets.optionalAttrs (pkgs.qchem.gaussian != null) {
          inherit (pkgs.qchem) gaussian;
        };

        legacyPackages.submit = {
          nwchem = pkgs.callPackage ./pkgs/submit/nwchem.nix { };
          orca = pkgs.callPackage ./pkgs/submit/orca.nix { };
          psi4 = pkgs.callPackage ./pkgs/submit/psi4.nix { };
          cp2k = pkgs.callPackage ./pkgs/submit/cp2k.nix { };
          gaussian = pkgs.callPackage ./pkgs/submit/gaussian.nix { };
          pysisyphus = pkgs.callPackage ./pkgs/submit/pysisyphus.nix { };
          xtb = pkgs.callPackage ./pkgs/submit/xtb.nix { };
        };

        bundlers = {
          inherit toSingularityImage;
          inherit (nixBundlers.bundlers."${system}") toArx toDEB toRPM toDockerImage;
          default = self.bundlers."${system}".toArx;
        };

        checks = self.packages."${system}";
      }
    ) // {
    hydraJobs."x86_64-linux" = self.checks."x86_64-linux";
  };
}
