{
  description = "Nix templates for computational chemistry";

  inputs = {
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    nixgl.url = "github:nix-community/nixgl";
  };

  outputs = { self, nixpkgs, nixgl }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
        overlays = [ nixgl.overlays.default ];
        config.allowUnfree = true;
      };
    in
    {
      # NixGL for Ara. Version must match the Nvidia driver installed on Ara.
      # Check using nvidia-smi. Requires impure install
      packages.x86_64-linux.nixGLAra =
        (pkgs.nixgl.override {
          nvidiaVersion = "550.54.14";
        }).auto.nixGLNvidia;

      templates = {
        computationalChemistry = {
          path = ./computationalChemistry;
          description = "A flake for computational chemistry, both quantum and classical";
          welcomeText = ''
            Run "nix flake metadata" to get additional information about this
            flake. Additionally, check the configuration values in "qchem-config".          
            Defaults are usually fine, you may still want to tweak some, though.
          '';
        };

        haskell = {
          path = ./haskell;
          description = "A opinionated flake for Haskell development";
          welcomeText = ''
            Replace all occurences of `package` by the actual name of your package.
          '';
        };

        fortran = {
          path = ./fortran;
          description = "A flake for Fortran development";
          welcomeText = ''
            Use './setup.sh <myPackageName>' to initialize your repository to your
            preferred name.

            This template uses Meson to build. You will have to change the default
            'meson.build' as you add new source files. Best practice is with the
            following command:

            meson init -f -n <myProjectName> -e <myExecutableName> main.f90 src/*
          '';
        };
      };

      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;

      devShells.x86_64-linux.default = with nixpkgs.legacyPackages.x86_64-linux;
        mkShell { buildInputs = [ nixpkgs-fmt ]; };
    };
}
