MODULE HELLO
  IMPLICIT NONE
  CONTAINS

    SUBROUTINE helloWorld()
      PRINT *, 'HELLO WORLD'
    END SUBROUTINE helloWorld

END MODULE HELLO
