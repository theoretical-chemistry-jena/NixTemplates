#!/usr/bin/env bash

# This file sets up the template by setting the name of the project in all
# relevant spot. Use as 'setup.sh myProjectName'.

sed -i s/@template@/$1/g flake.nix

sed -i s/@template@/$1/g meson.build
