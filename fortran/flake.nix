{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    flake-utils.url = "github:numtide/flake-utils/main";

    nixBundlers.url = "github:NixOS/bundlers/master";
  };

  nixConfig.bash-prompt = ''\[\e[0;1;34m\][Fortran:\w]$\[\e[0m\] '';

  outputs = { self, nixpkgs, flake-utils, nixBundlers }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          packageName = "@template@";
          pkgs = import nixpkgs { inherit system; };
        in
        {
          packages.default = self.packages."${system}".${packageName};

          packages.${packageName} = with pkgs; stdenv.mkDerivation {
            name = packageName;
            src = ./.;
            buildInputs = [ ];
            nativeBuildInputs = [
              gfortran
              meson
              ninja
            ];
          };

          devShells.default = self.devShells."${system}".${packageName};

          devShells.${packageName} = pkgs.mkShell {
            inputsFrom = [ self.packages."${system}".${packageName} ];
            nativeBuildInputs = with pkgs; [
              fprettify
            ];
          };

          bundlers = {
            inherit (nixBundlers.bundlers."${system}") toArx toDEB toRPM toDockerImage;
            default = self.bundlers."${system}".toArx;
          };

          checks = self.packages."${system}";
        }
      ) // {
      hydraJobs."x86_64-linux" = self.checks."x86_64-linux";
    };
}
